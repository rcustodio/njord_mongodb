from tornpack.options import options

__all__ = [
    'db_encode',
    'env_encode',
    'ns_encode'
]

def db_encode(db,collection):
    return options.njord_mongodb_encode['db'] % (db,collection)

def env_encode(ns):
    return options.njord_mongodb_encode['env'] % (options.njord_mongodb_env,ns)

def ns_encode(_,service):
    return options.njord_mongodb_encode['ns'] % (_,service)

def path_decode(_):
    try:
        assert _
        db, collection = _.split('.')[0:2]
    except:
        return False
    return {'db':db,'collection':collection}

def rabbitmq_queue_encode(_):
    return options.njord_mongodb_encode['rmq']['queue'] % (
        _,
        options.njord_mongodb_env,
        options.njord_mongodb_cluster
    )
