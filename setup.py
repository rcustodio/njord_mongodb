from announce import Announce
from static import env_encode as ENVENCODE, rabbitmq_queue_encode as RMQENCODE
from tornpack.actor.base import Base
from tornpack.mongodb import Mongodb
from tornpack.options import options
from .aggregate import Aggregate
from .cursor.close import Close as CursorClose
from .cursor.distinct import Distinct as CursorDistinct
from .cursor.fetch import Fetch as CursorFetch
from .find import Find
from .find_and_modify import FindAndModify
from .find_one import FindOne
from .get import Get
from .insert import Insert
from .remove import Remove
from .save import Save
from .update import Update

__all__ = ['Setup']

class Setup(Base):
    __announce = None

    def __init__(self):
        try:
            assert 'njord_mongodb' in options
        except AssertionError:
            pass
        except:
            raise
        else:
            self.__announce = Announce().push
            self.ioengine.ioloop.add_callback(self.services)
            self.ioengine.ioloop.add_callback(self.mongodb)

    def mongodb(self,i=None):
        try:
            name = i.next()
        except AttributeError:
            self.ioengine.ioloop.add_callback(self.mongodb,i=iter(options.njord_mongodb))
        except StopIteration:
            return True
        except:
            raise
        else:
            Mongodb.engine_open(
                name=ENVENCODE(name),
                url=options.njord_mongodb[name]['url']
            )
            self.ioengine.ioloop.add_callback(
                self.__announce,
                db=name,
                collection=options.njord_mongodb[name]['collection']
            )
            self.ioengine.ioloop.add_callback(self.mongodb,i=i)
        return None

    def service_aggregate(self):
        try:
            assert options.njord_mongodb_services['aggregate']
        except:
            pass
        else:
            Aggregate(name=RMQENCODE(options.njord_mongodb_services['aggregate']))
        return True

    def service_cursor(self):
        self.ioengine.ioloop.add_callback(self.service_cursor_close)
        self.ioengine.ioloop.add_callback(self.service_cursor_fetch)
        self.ioengine.ioloop.add_callback(self.service_cursor_distinct)
        return True

    def service_cursor_close(self):
        try:
            assert options.njord_mongodb_cursor_services['close']
        except:
            pass
        else:
            CursorClose(name=RMQENCODE(options.njord_mongodb_cursor_services['close']))
        return True

    def service_cursor_distinct(self):
        try:
            assert options.njord_mongodb_cursor_services['distinct']
        except:
            pass
        else:
            CursorDistinct(name=RMQENCODE(options.njord_mongodb_cursor_services['distinct']))
        return True

    def service_cursor_fetch(self):
        try:
            assert options.njord_mongodb_cursor_services['fetch']
        except:
            pass
        else:
            CursorFetch(name=RMQENCODE(options.njord_mongodb_cursor_services['fetch']))
        return True

    def service_find(self):
        try:
            assert options.njord_mongodb_services['find']
        except (AssertionError,KeyError):
            pass
        except:
            raise
        else:
            Find(name=RMQENCODE(options.njord_mongodb_services['find']))
        return True

    def service_find_and_modify(self):
        try:
            assert options.njord_mongodb_services['find_and_modify']
        except (AssertionError,KeyError):
            pass
        except:
            raise
        else:
            FindAndModify(name=RMQENCODE(options.njord_mongodb_services['find_and_modify']))
        return True

    def service_find_one(self):
        try:
            assert options.njord_mongodb_services['find_one']
        except (AssertionError,KeyError):
            pass
        except:
            raise
        else:
            FindOne(name=RMQENCODE(options.njord_mongodb_services['find_one']))
        return True

    def service_get(self):
        try:
            assert options.njord_mongodb_services['get']
        except (AssertionError,KeyError):
            pass
        except:
            raise
        else:
            Get(name=RMQENCODE(options.njord_mongodb_services['get']))
        return True

    def service_insert(self):
        try:
            assert options.njord_mongodb_services['insert']
        except (AssertionError,KeyError):
            pass
        except:
            raise
        else:
            Insert(name=RMQENCODE(options.njord_mongodb_services['insert']))
        return True

    def service_remove(self):
        try:
            assert options.njord_mongodb_services['remove']
        except (AssertionError,KeyError):
            pass
        except:
            raise
        else:
            Remove(name=RMQENCODE(options.njord_mongodb_services['remove']))
        return True

    def service_save(self):
        try:
            assert options.njord_mongodb_services['save']
        except (AssertionError,KeyError):
            pass
        except:
            raise
        else:
            Save(name=RMQENCODE(options.njord_mongodb_services['save']))
        return True

    def service_update(self):
        try:
            assert options.njord_mongodb_services['update']
        except (AssertionError,KeyError):
            pass
        except:
            raise
        else:
            Update(name=RMQENCODE(options.njord_mongodb_services['update']))
        return True

    def services(self):
        self.ioengine.ioloop.add_callback(self.service_aggregate)
        self.ioengine.ioloop.add_callback(self.service_cursor)
        self.ioengine.ioloop.add_callback(self.service_find)
        self.ioengine.ioloop.add_callback(self.service_find_and_modify)
        self.ioengine.ioloop.add_callback(self.service_find_one)
        self.ioengine.ioloop.add_callback(self.service_get)
        self.ioengine.ioloop.add_callback(self.service_insert)
        self.ioengine.ioloop.add_callback(self.service_remove)
        self.ioengine.ioloop.add_callback(self.service_save)
        self.ioengine.ioloop.add_callback(self.service_update)
        return True

Setup()
