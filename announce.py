from static import db_encode as DBENCODER, env_encode as ENVENCODER, ns_encode as NSENCODER, rabbitmq_queue_encode as RMQENCODE
from tornpack.actor.rabbitmq.channel import Channel
from tornpack.actor.rabbitmq.connection import Connection
from tornpack.actor.rabbitmq.exchange import Exchange
from tornpack.actor.rabbitmq.queue import Queue
from tornpack.options import options

__all__ = ['Announce']

class Announce(Channel,Connection,Exchange,Queue):   
    def __init__(self):
        self.name = self.uid
        self.ioengine.ioloop.add_callback(self.__rabbitmq_connection__)

    def __on_rabbitmq_channel__(self,channel):
        self.is_ready = True
        return True
        
    def __on_rabbitmq_connection__(self,connection):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_channel__)
        return True

    def bind(self,ns,service):
        def on_bind(result):
            return True
        
        self.__rabbitmq_queue_bind__(
            queue=self.__rabbitmq_queue_name__(RMQENCODE(options.njord_mongodb_services[service])),
            exchange=options.tornpack_rabbitmq_exchange['headers_any'],
            routing_key='',
            arguments={'mongodb_path':ns},
            future=self.ioengine.future_instance(on_bind)
        )
        return True
    
    def encode_db(self,db,collection):
        self.ioengine.ioloop.add_callback(
            self.encode_service,
            ns=ENVENCODER(DBENCODER(db,collection))
        )
        return True

    def encode_service(self,ns,service=None):
        try:
            i = service.next()
        except AttributeError:
            self.ioengine.ioloop.add_callback(
                self.encode_service,
                ns=ns,
                service=iter(options.njord_mongodb_services)
            )
        except StopIteration:
            return True
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(
                self.bind,
                ns=NSENCODER(ns,i),
                service=i
            )
            self.ioengine.ioloop.add_callback(
                self.encode_service,
                ns=ns,
                service=service
            )
        return None
    
    def push(self,db,collection):
        try:
            assert self.io
        except AssertionError:
            def again(result):
                self.ioengine.ioloop.add_callback(self.push,db=db,collection=collection)
                return True

            self.promises.append(self.ioengine.future_instance(again))
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(
                self.sequence,
                db=db,
                collection=collection
            )
        return True

    def sequence(self,db,collection):
        try:
            c = collection.next()
        except AttributeError:
            self.ioengine.ioloop.add_callback(
                self.sequence,
                db=db,
                collection=iter(collection)
            )
        except StopIteration:
            return True
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(
                self.encode_db,
                db=db,
                collection=c
            )
            self.ioengine.ioloop.add_callback(
                self.sequence,
                db=db,
                collection=collection
            )
        return None
