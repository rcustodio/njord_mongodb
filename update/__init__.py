from njord_mongodb.static import path_decode as PATHDECODER
from pika import BasicProperties
from tornpack.actor.mdb import MDB
from tornpack.actor.rabbitmq import SimpleConsumer
from tornpack.parser.json import dictfy,jsonify
from tornpack.options import options

__all__ = ['Update']

class Update(SimpleConsumer,MDB):
    def db_open(self,msg):
        def on_db(result):
            self.ioengine.ioloop.add_callback(
                self.update,
                db=result.result(),
                msg=msg
            )
            return True

        self.db(
            name=msg['mongodb_path']['db'],
            db=msg['mongodb_path']['db'],
            collection=msg['mongodb_path']['collection'],
            future=self.ioengine.future_instance(on_db)
        )
        return True

    def on_message(self,msg):
        try:
            assert msg['properties'].headers['etag']
            msg['body'] = dictfy(msg['body'])
            assert msg['body']
            assert msg['body']['query']
            assert msg['body']['document']
            msg['mongodb_path'] = PATHDECODER(msg['properties'].headers['mongodb_path'])
            assert msg['mongodb_path']
        except (AssertionError,KeyError):
            msg['ack'].set_result(True)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.db_open,msg=msg)
        return True

    def response(self,msg,code,body=''):
        self.rabbitmq_channel.basic_publish(
            body=body,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key='',
            properties=BasicProperties(headers={
                'code':code,
                'etag':msg['properties'].headers['etag']
            })
        )
        msg['ack'].set_result(True)
        return True

    def update(self,db,msg):
        def on_update(result):
            try:
                assert result.result()
            except AssertionError:
                self.ioengine.ioloop.add_callback(
                    self.response,
                    msg=msg,
                    code=options.njord_mongodb_codes['update']['nok']
                )
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(
                    self.response,
                    msg=msg,
                    code=options.njord_mongodb_codes['update']['ok'],
                    body=jsonify(result.result())
                )
            return True

        db.update(
            msg['body']['query'],
            msg['body']['document'],
            future=self.ioengine.future_instance(on_update),
            upsert=msg['properties'].headers.get('upsert',False)
        )
        return True
