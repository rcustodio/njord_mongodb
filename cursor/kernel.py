__all__ = ['Kernel']

class Kernel(object):
    __connections = None

    @property
    def connections(self):
        try:
            assert self.__connections
        except AssertionError:
            self.__connections = {}
        except:
            raise
        return self.__connections

    def connection_add(self,io):
        self.connections[io.uid] = io
        return True

    def connection_del(self,io):
        try:
            assert io.uid in self.connections
        except AssertionError:
            pass
        except:
            raise
        else:
            del self.connections[io.uid]
        return True

Kernel = Kernel()
