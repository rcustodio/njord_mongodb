from njord_mongodb.cursor.kernel import Kernel
from tornpack.actor.base import Base
from tornpack.options import options

__all__ = ['Handler']

class Handler(Base):
    __alive_time = None
    __connections_waiting = None
    __cursor = None
    __fetching = False
    __future = None
    __idle_timeout = None

    @property
    def alive_time(self):
        try:
            assert self.__alive_time
        except AssertionError:
            self.__alive_time = options.njord_mongodb_cursor_alive_time
        except:
            raise
        return self.__alive_time

    @property
    def connections_waiting(self):
        try:
            assert self.__connections_waiting
        except AssertionError:
            self.__connections_waiting = []
        except:
            raise
        return self.__connections_waiting

    def __fetch_one(self,future):
        def on_fetch(result):
            try:
                assert result.result()
            except AssertionError:
                future.set_exception(StopIteration())
                self.ioengine.ioloop.add_callback(self.close)
            except:
                raise
            else:
                future.set_result(self.__cursor.next_object())

            self.ioengine.ioloop.add_callback(self.__idle)
            self.ioengine.ioloop.add_callback(self.__next_connection)
            return True

        self.ioengine.ioloop.add_future(self.__cursor.fetch_next,on_fetch)
        return True

    def __idle(self):
        try:
            assert self.__idle_timeout
        except AssertionError:
            pass
        except:
            raise
        else:
            self.ioengine.ioloop.remove_timeout(self.__idle_timeout)

        self.__idle_timeout = self.ioengine.ioloop.call_later(
            self.alive_time.total_seconds(),
            self.close
        )
        return True

    def __next_connection(self):
        try:
            self.__fetch_one(self.connections_waiting.pop(0))
        except IndexError:
            self.__fetching = False
        except:
            raise
        return True

    def __init__(self,cursor,future=None,alive_time=None):
        self.__alive_time = alive_time
        self.__cursor = cursor
        self.__future = future
        Kernel.connection_add(self)
        self.ioengine.ioloop.add_callback(self.__idle)

    def close(self,future=None):
        def on_close(result,err):
            try:
                assert future
            except AssertionError:
                pass
            except:
                raise
            else:
                future.set_result(result)
            return True

        self.__cursor.close(callback=on_close)
        Kernel.connection_del(self)
        return True

    def distinct(self,key,future):
        def on_dist(result,error):
            try:
                assert error
            except AssertionError:
                future.set_result(result)
            except:
                raise
            else:
                future.set_exception(error)
            return True

        self.__cursor.distinct(key,callback=on_dist)
        return True

    def next(self,future):
        try:
            assert self.__fetching
        except AssertionError:
            self.__fetching = True
            self.__fetch_one(future)
        except:
            raise
        else:
            self.connections_waiting.append(future)
        return True
