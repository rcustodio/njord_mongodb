from njord_mongodb.cursor.kernel import Kernel
from pika import BasicProperties
from tornpack.actor.mdb import MDB
from tornpack.actor.rabbitmq import SimpleConsumer
from tornpack.options import options
from tornpack.parser.json import jsonify

__all__ = ['Close']

class Close(SimpleConsumer,MDB):
    @property
    def queue(self):
        return self.__rabbitmq_queue_name__('%s_%s' % (self.name,options.tornpack_name))

    def __on_rabbitmq_queue_declare__(self,queue):
        self.__rabbitmq_queue_bind__(
            arguments={
                'service':options.tornpack_name,
                'action':options.njord_mongodb_cursor_services['close']
            },
            exchange=options.tornpack_rabbitmq_exchange['headers'],
            queue=self.queue,
            routing_key=''
        )
        return True

    def close(self,msg):
        def on_close(result):
            self.rabbitmq_channel.basic_publish(
                body='',
                exchange=options.tornpack_rabbitmq_exchange['default'],
                routing_key='',
                properties=BasicProperties(headers={
                    'code':options.njord_mongodb_codes['cursor']['close']['ok'],
                    'etag':msg['properties'].headers['etag']
                })
            )
            msg['ack'].set_result(True)
            return True

        try:
            io = Kernel.connections[msg['properties'].headers['cursor_id']]
        except KeyError:
            on_close(None)
        except:
            raise
        else:
            io.close(self.ioengine.future_instance(on_close))
        return True

    def on_message(self,msg):
        try:
            assert msg['properties'].headers['etag']
            assert msg['properties'].headers['cursor_id']
        except (AssertionError,KeyError):
            msg['ack'].set_result(True)
        except:
            raise
        else:
            msg['swap'] = {}
            self.ioengine.ioloop.add_callback(self.close,msg=msg)
        return True
