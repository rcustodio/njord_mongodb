from njord_mongodb.cursor.kernel import Kernel
from pika import BasicProperties
from tornpack.actor.rabbitmq import SimpleConsumer
from tornpack.options import options
from tornpack.parser.json import jsonify

__all__ = ['Distinct']

class Distinct(SimpleConsumer):
    @property
    def queue(self):
        return self.__rabbitmq_queue_name__('%s_%s' % (self.name,options.tornpack_name))

    def __on_rabbitmq_queue_declare__(self,queue):
        self.__rabbitmq_queue_bind__(
            arguments={
                'service':options.tornpack_name,
                'action':options.njord_mongodb_cursor_services['distinct']
            },
            exchange=options.tornpack_rabbitmq_exchange['headers'],
            queue=self.queue,
            routing_key=''
        )
        return True

    def get(self,msg):
        try:
            io = Kernel.connections[msg['properties'].headers['cursor_id']]
        except KeyError:
            self.ioengine.ioloop.add_callback(
                self.respond,
                code=options.njord_mongodb_codes['cursor']['distinct']['nok'],
                msg=msg
            )
        except:
            raise
        else:
            def on_fetch(result):
                try:
                    self.ioengine.ioloop.add_callback(
                        self.respond,
                        code=options.njord_mongodb_codes['cursor']['distinct']['ok'],
                        msg=msg,
                        body=jsonify(result.result())
                    )
                except TypeError:
                    self.ioengine.ioloop.add_callback(
                        self.respond,
                        code=options.njord_mongodb_codes['cursor']['distinct']['payload_error'],
                        msg=msg
                    )
                except:
                    raise
                return True

            io.distinct(
                msg['properties'].headers['key'],
                self.ioengine.future_instance(on_fetch)
            )
        return True

    def on_message(self,msg):
        try:
            assert msg['properties'].headers['etag']
            assert msg['properties'].headers['cursor_id']
            assert msg['properties'].headers['key']
        except (AssertionError,KeyError):
            msg['ack'].set_result(True)
        except:
            raise
        else:
            msg['swap'] = {}
            self.ioengine.ioloop.add_callback(self.get,msg=msg)
        return True

    def respond(self,code,msg,body=''):
        self.rabbitmq_channel.basic_publish(
            body=body,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key='',
            properties=BasicProperties(headers={
                'etag':msg['properties'].headers['etag'],
                'code':code
            })
        )

        msg['ack'].set_result(True)
        return True
