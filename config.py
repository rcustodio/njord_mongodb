from datetime import timedelta
from tornpack.options import define,options
from tornpack.raven import Raven

define('njord_mongodb_cluster',default='vm')
define('njord_mongodb_codes',default={
    'aggregate':{
        'nok':'0e157026c8b94ad9b9d9325b9aca7645',
        'ok':'9fe70aecd6de42a69b0f38bfda220b2a',
        'pipeline_error':'078c2877965147cda54bc4cfb79b2d82'
    },
    'cursor':{
        'close':{
            'ok':'d8973e22332a425ba6908dc3b93ff0ff'
        },
        'distinct':{
            'nok':'f0b3aa34482745dd90d6834d901cdc64',
            'ok':'0372ad8ca39e401f84a9a1f869ad9354',
            'payload_error':'e30ae9d5d24549dfb1dd098c465e77ca'
        },
        'fetch':{
            'empty':'54279f501af441a48d25a2485d64d0a2',
            'nok':'b9c1a9cf3bb841fd92214f3d7196cf81',
            'ok':'3aa6c5224bab44f39158d09d38f1e51b'
        }
    },
    'find':{
        'nok':'70cc2113d183453991d1b6af080b481a',
        'ok':'60221ca134b243c2af4c70a391b07c70'
    },
    'find_and_modify':{
        'nok':'5c458eaf8ec34b47a33bbb565e16937c',
        'ok':'c24c11edd77a45669a48ef01584f55d4'
    },
    'find_one':{
        'nok':'cfc00c72a41a47fe8bd79b350eadf24c',
        'ok':'c105b0f20d90415b89433a69d305c551'
    },
    'get':{
        'nok':'52408aa344704525804a9870c1c77eea',
        'ok':'255eb965d2374b37915043e7f1b00f43'
    },
    'insert':{
        'nok':'a949189f773b44cabc52eaa1ee20dfe2',
        'ok':'9ea54cd929954243994846d256d60d8e'
    },
    'remove':{
        'nok':'5326debbb16f47d68705daf43fe8b1b3',
        'ok':'65c1a4e2ee274902a934b66836dc6cec'
    },
    'save':{
        'nok':'5b1bd725661b49c58cbdf0ebb9da3308',
        'ok':'36c8d32e4a7c412792560f273e5f21ff',
        'validate_error':'c27b673f7c0a4724a4c6531ca1ce423e'
    },
    'update':{
        'nok':'21f0f999d7134c579a38bc18629b8c82',
        'ok':'9c52571596aa488da027f8c8fbd2dbbf'
    }
})
define('njord_mongodb_encode',default={
    'db':'%s.%s',
    'env':'%s_%s',
    'ns':'%s.%s',
    'rmq':{
        'queue':'%s_%s_%s'
    }
})

define('njord_mongodb_cursor_services',default={
    'close':'njord_mongodb_cursor_close',
    'distinct':'njord_mongodb_cursor_distinct',
    'fetch':'njord_mongodb_cursor_fetch'
})
define('njord_mongodb_services',default={
    'aggregate':'njord_mongodb_aggregate',
    'find':'njord_mongodb_find',
    'find_and_modify':'njord_mongodb_find_and_modify',
    'find_one':'njord_mongodb_find_one',
    'get':'njord_mongodb_get',
    'insert':'njord_mongodb_insert',
    'remove':'njord_mongodb_remove',
    'save':'njord_mongodb_save',
    'update':'njord_mongodb_update'
})

define('njord_mongodb_cursor_alive_time',default=timedelta(minutes=10))

try:
    assert 'njord_mongodb' in options
except AssertionError:
    Raven.warning('option njord_mongodb needed')
except:
    raise

try:
    assert 'njord_mongodb_env' in options
except AssertionError:
    define('njord_mongodb_env',default='instability')
    Raven.warning('option njord_mongodb_env not defined, using default "%s"' % options.njord_mongodb_env)
except:
    raise
