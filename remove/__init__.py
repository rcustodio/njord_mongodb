from njord_mongodb.static import path_decode as PATHDECODER
from pika import BasicProperties
from tornpack.actor.mdb import MDB
from tornpack.actor.rabbitmq import SimpleConsumer
from tornpack.parser.json import dictfy
from tornpack.options import options

__all__ = ['Remove']

class Remove(SimpleConsumer,MDB):
    def db_open(self,msg):
        def on_db(result):
            self.ioengine.ioloop.add_callback(
                self.remove,
                db=result.result(),
                msg=msg
            )
            return True

        self.db(
            name=msg['mongodb_path']['db'],
            db=msg['mongodb_path']['db'],
            collection=msg['mongodb_path']['collection'],
            future=self.ioengine.future_instance(on_db)
        )
        return True

    def on_message(self,msg):
        try:
            assert msg['properties'].headers['etag']
            msg['body'] = dictfy(msg['body'])
            assert msg['body']
            msg['mongodb_path'] = PATHDECODER(msg['properties'].headers['mongodb_path'])
            assert msg['mongodb_path']
        except (AssertionError,KeyError):
            msg['ack'].set_result(True)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.db_open,msg=msg)
        return True

    def remove(self,db,msg):
        def on_remove(result):
            try:
                assert result.result()
            except AssertionError:
                code = options.njord_mongodb_codes['remove']['nok']
            except:
                raise
            else:
                code = options.njord_mongodb_codes['remove']['ok']

            self.ioengine.ioloop.add_callback(
                self.response,
                msg=msg,
                code=code
            )
            return True

        db.remove(
            msg['body'],
            future=self.ioengine.future_instance(on_remove),
            multi=msg['properties'].headers.get('multi',False)
        )
        return True

    def response(self,msg,code):
        self.rabbitmq_channel.basic_publish(
            body='',
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key='',
            properties=BasicProperties(headers={
                'code':code,
                'etag':msg['properties'].headers['etag']
            })
        )
        msg['ack'].set_result(True)
        return True
