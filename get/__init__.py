from njord_mongodb.static import path_decode as PATHDECODER
from pika import BasicProperties
from tornpack.actor.mdb import MDB
from tornpack.actor.rabbitmq import SimpleConsumer
from tornpack.parser.json import dictfy,jsonify
from tornpack.options import options

__all__ = ['Get']

class Get(SimpleConsumer,MDB):
    def db_open(self,msg):
        def on_db(result):
            self.ioengine.ioloop.add_callback(
                self.get,
                db=result.result(),
                msg=msg
            )
            return True

        self.db(
            name=msg['mongodb_path']['db'],
            db=msg['mongodb_path']['db'],
            collection=msg['mongodb_path']['collection'],
            future=self.ioengine.future_instance(on_db)
        )
        return True

    def get(self,db,msg):
        def on_get(result):
            try:
                assert result.result()
            except (AssertionError,KeyError):
                body = ''
                code = options.njord_mongodb_codes['get']['nok']
            except:
                raise
            else:
                body = jsonify(result.result())
                code = options.njord_mongodb_codes['get']['ok']

            self.ioengine.ioloop.add_callback(
                self.response,
                msg=msg,
                code=code,
                body=body
            )
            return True

        db.find_one({'_id':msg['properties'].headers['mongodb_id']},future=self.ioengine.future_instance(on_get))
        return True

    def on_message(self,msg):
        try:
            assert msg['properties'].headers['etag']
            msg['mongodb_path'] = PATHDECODER(msg['properties'].headers['mongodb_path'])
            assert msg['mongodb_path']
            assert msg['properties'].headers['mongodb_id']
        except (AssertionError,KeyError):
            msg['ack'].set_result(True)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.db_open,msg=msg)
        return True

    def response(self,msg,code,body=''):
        self.rabbitmq_channel.basic_publish(
            body=body,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key='',
            properties=BasicProperties(headers={
                'code':code,
                'etag':msg['properties'].headers['etag']
            })
        )
        msg['ack'].set_result(True)
        return True
