from hashlib import sha1
from njord_mongodb.static import path_decode as PATHDECODER
from pika import BasicProperties
from tornpack.actor.mdb import MDB
from tornpack.actor.rabbitmq import SimpleConsumer
from tornpack.parser.json import dictfy,jsonify
from tornpack.options import options

__all__ = ['Save']

class Save(SimpleConsumer,MDB):
    @property
    def _id(self):
        return sha1('%s<%s>' % (self.ioengine.uuid4,self.ioengine.ioloop.time())).hexdigest()

    def check_id(self,msg):
        try:
            assert msg['body']['_id']
        except (AssertionError,KeyError):
            msg['body']['_id'] = self._id
        except:
            raise

        self.ioengine.ioloop.add_callback(self.db_open,msg=msg)
        return True

    def db_open(self,msg):
        def on_db(result):
            self.ioengine.ioloop.add_callback(
                self.save,
                db=result.result(),
                msg=msg
            )
            return True

        self.db(
            name=msg['mongodb_path']['db'],
            db=msg['mongodb_path']['db'],
            collection=msg['mongodb_path']['collection'],
            future=self.ioengine.future_instance(on_db)
        )
        return True

    def on_message(self,msg):
        try:
            assert msg['properties'].headers['etag']
            msg['body'] = dictfy(msg['body'])
            assert msg['body']
            msg['mongodb_path'] = PATHDECODER(msg['properties'].headers['mongodb_path'])
            assert msg['mongodb_path']
        except (AssertionError,KeyError):
            msg['ack'].set_result(True)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.check_id,msg=msg)
        return True

    def response(self,code,msg,body=''):
        self.rabbitmq_channel.basic_publish(
            body=body,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key='',
            properties=BasicProperties(headers={
                'code':code,
                'etag':msg['properties'].headers['etag']
            })
        )
        msg['ack'].set_result(True)
        return True

    def save(self,db,msg):
        def on_save(result):
            try:
                assert result.result()
            except AssertionError:
                body = ''
                code = options.njord_mongodb_codes['save']['nok']
            except:
                raise
            else:
                body = jsonify({'_id':result.result()})
                code = options.njord_mongodb_codes['save']['ok']

            self.ioengine.ioloop.add_callback(
                self.response,
                body=body,
                code=code,
                msg=msg
            )
            return True

        db.save(msg['body'],future=self.ioengine.future_instance(on_save))
        return True
