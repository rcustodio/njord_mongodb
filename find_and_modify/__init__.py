from njord_mongodb.static import path_decode as PATHDECODER
from pika import BasicProperties
from tornpack.actor.mdb import MDB
from tornpack.actor.rabbitmq import SimpleConsumer
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['FindAndModify']

class FindAndModify(SimpleConsumer,MDB):
    def db_open(self,msg):
        def on_db(result):
            self.ioengine.ioloop.add_callback(
                self.find,
                db=result.result(),
                msg=msg
            )
            return True

        self.db(
            name=msg['mongodb_path']['db'],
            db=msg['mongodb_path']['db'],
            collection=msg['mongodb_path']['collection'],
            future=self.ioengine.future_instance(on_db)
        )
        return True

    def find(self,db,msg):
        def on_find(result):
            try:
                assert result.result()
            except AssertionError:
                self.ioengine.ioloop.add_callback(
                    self.respond,
                    code=options.njord_mongodb_codes['find_and_modify']['nok'],
                    msg=msg
                )
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(
                    self.respond,
                    code=options.njord_mongodb_codes['find_and_modify']['ok'],
                    msg=msg,
                    body=jsonify(result.result())
                )
            return True

        db.find_and_modify(
            query=msg['body']['query'],
            document=msg['body']['document'],
            op=msg['body']['op'],
            fields=msg['properties'].headers.get('fields'),
            skip=msg['properties'].headers.get('skip',0),
            sort=msg['properties'].headers.get('sort'),
            new=msg['properties'].headers.get('new',False),
            upsert=msg['properties'].headers.get('upsert',False),
            future=self.ioengine.future_instance(on_find)
        )
        return True

    def on_message(self,msg):
        try:
            assert msg['properties'].headers['etag']
            msg['body'] = dictfy(msg['body'])
            assert msg['body']
            assert 'query' in msg['body']
            assert msg['body']['document']
            assert msg['body']['op'] in ('remove','update')
            msg['mongodb_path'] = PATHDECODER(msg['properties'].headers['mongodb_path'])
            assert msg['mongodb_path']
        except (AssertionError,KeyError):
            msg['ack'].set_result(True)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.db_open,msg=msg)
        return True

    def respond(self,code,msg,body=''):
        self.rabbitmq_channel.basic_publish(
            body=body,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key='',
            properties=BasicProperties(headers={
                'code':code,
                'etag':msg['properties'].headers['etag']
            })
        )
        msg['ack'].set_result(True)
        return True
